'use strict'

const Database = use('Database')

class LivroTitulo {
  async authorize() {
    const params = this.ctx.params
    const request = this.ctx.request
    const response = this.ctx.response
    const req = request.only(['titulo'])

    // Verifica se titulo do livro já existe
    if (req.titulo) {
      var livro
      if (!params.id) { // Store
        livro = await Database.raw(`
          SELECT COUNT(c.id) AS qtd
          FROM livros AS c
          WHERE lower(c.titulo) = lower(?)
        `, [req.titulo])
      } else { // Update
        livro = await Database.raw(`
          SELECT COUNT(c.id) AS qtd
          FROM livros AS c
          WHERE c.id <> ? AND lower(c.titulo) = lower(?)
        `, [params.id, req.titulo])
      }
      if (livro.rows[0].qtd !== '0') {
        response.unauthorized({ error: 'titulo_existe' })
        return false
      }
    }

    return true
  }
}

module.exports = LivroTitulo
