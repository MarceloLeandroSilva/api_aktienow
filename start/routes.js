'use strict'

const Route = use('Route')

Route.post('/users', 'UserController.create')
Route.post('/sessions', 'SessionController.create')

//aplica o validator nas rotas de atualizacao e insercao de livros
Route.post('livros', 'LivroController.store')
  .validator('LivroTitulo')
  .middleware('auth')
Route.put('livros/:id', 'LivroController.update')
  .validator('LivroTitulo')
  .middleware('auth')
Route.get('livros', 'LivroController.index')
  .middleware('auth')
Route.get('livros/:id', 'LivroController.show')
  .middleware('auth')
Route.delete('livros/:id', 'LivroController.destroy')
  .middleware('auth')
Route.post('livros/:id/images', 'ImageController.store')
  .middleware('auth')


Route.resource('generos', 'GeneroController')
  .apiOnly()
  .middleware('auth')

Route.resource('avaliacoes', 'AvaliacaoController')
  .apiOnly()
  .middleware('auth')
Route.get('avaliacoes/livro/:id/', 'AvaliacaoController.indexPlivro')
  .middleware('auth')

Route.get('images/:path', 'ImageController.show')
